from django.db import models
from django import forms
from .models import ActivityModel


import datetime

class ActivityForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs = {
            "placeholder": 'Activity'
        }
    ))
    category = forms.CharField(widget=forms.TextInput(
        attrs = {
            "placeholder": 'Category'
        }
    ))
    date = forms.DateField(input_formats=['%Y-%m-%d'], widget=forms.TextInput(
        attrs = {
            "placeholder": 'YYYY-MM-DD'
        }
    ))
    hour = forms.TimeField(input_formats=['%H:%M'], widget=forms.TimeInput(
        attrs = {
            "placeholder": 'Hour:Minute'
        }
    ))


    class Meta:
        model = ActivityModel
        fields = ("name", "category", "date", "hour")
        labels ={
            "name": "Activity Name",
            "category": "Activity Category",
            "date": "Date",
            "hour": "time"
        }
