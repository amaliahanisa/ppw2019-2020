from django.shortcuts import render
from .models import ActivityModel
from .forms import ActivityForm
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages

# Create your views here.

def index(request):
    return render(request, 'index.html')

# def contact(request):
#     return render(request, 'contact.html')

# def profile(request):
#     return render(request, 'profile.html')

def show(request):
    data = ActivityModel.objects.all()
    return render(request, 'form_result.html', {'data':data})

def warning(request):
    return render(request, 'invalid.html')   
 
def form(request):
    if request.method == "POST":
        data = ActivityForm(request.POST)
        if data.is_valid():
            data.save()
        else:
            messages.warning(request, 'Data input is not valid, Please try again!')

    return render(request, 'form.html', {'form': ActivityForm()})

def delete_activity(request, pk):
    activity = get_object_or_404(ActivityModel, pk=pk)
    activity.delete()
    # messages.success(request, 'You have successfully delete the activity')
    form = ActivityForm(instance=activity)

    # THIS DOESNT WORRRRRRRRRRRRRRRRRRRK
    # if request.method == "POST":
    #     activity.delete()
    #     messages.success(request, 'You have successfully delete the activity')
    # else:
    #     form = ActivityForm(instance=activity)

    context = {
        'form' : form,
        'activity' : activity,
    }
    return render (request, 'form.html', context)