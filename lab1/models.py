from django.db import models

# Create your models here.
class ActivityModel(models.Model):
    date = models.DateField()
    hour = models.TimeField(auto_now_add=False)
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=50)